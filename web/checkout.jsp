        <%@include  file="header.html" %>  
            <!-- Start Cart Panel -->
            <div class="shopping__cart">
                <div class="shopping__cart__inner">
                    <div class="offsetmenu__close__btn">
                        <a href="#"><i class="zmdi zmdi-close"></i></a>
                    </div>
                    <div class="shp__cart__wrap">
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="#">
                                    <img src="images/product/sm-img/1.jpg" alt="product images">
                                </a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="product-details.html">BO&Play Wireless Speaker</a></h2>
                                <span class="quantity">QTY: 1</span>
                                <span class="shp__price">$105.00</span>
                            </div>
                            <div class="remove__btn">
                                <a href="#" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                        <div class="shp__single__product">
                            <div class="shp__pro__thumb">
                                <a href="#">
                                    <img src="images/product/sm-img/2.jpg" alt="product images">
                                </a>
                            </div>
                            <div class="shp__pro__details">
                                <h2><a href="product-details.html">Brone Candle</a></h2>
                                <span class="quantity">QTY: 1</span>
                                <span class="shp__price">$25.00</span>
                            </div>
                            <div class="remove__btn">
                                <a href="#" title="Remove this item"><i class="zmdi zmdi-close"></i></a>
                            </div>
                        </div>
                    </div>
                    <ul class="shoping__total">
                        <li class="subtotal">Subtotal:</li>
                        <li class="total__price">$130.00</li>
                    </ul>
                    <ul class="shopping__btn">
                        <li><a href="cart.jsp">Ver Carrito</a></li>
                        <li class="shp__checkout"><a href="checkout.jsp">Pagar</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Cart Panel -->
        </div>
        <!-- End Offset Wrapper -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area" style="background: rgba(0, 0, 0, 0) url(images/bg/2.jpg) no-repeat scroll center center / cover ;">
            <div class="ht__bradcaump__wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="bradcaump__inner text-center">
                                <h2 class="bradcaump-title">Finalizar Compra</h2>
                                <nav class="bradcaump-inner">
                                    <a class="breadcrumb-item" href="index.jsp">Inicio</a>
                                    <span class="brd-separetor">/</span>
                                    <span class="breadcrumb-item active">Formulario de Pago</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Checkout Area -->
        <section class="our-checkout-area ptb--120 bg__white">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <div class="ckeckout-left-sidebar">
                            <!-- Start Checkbox Area -->
                            <div class="checkout-form">
                                <h2 class="section-title-3">Detalles de facturaci�n</h2>
                                <div class="checkout-form-inner">
                                    <div class="single-checkout-box">
                                        <input type="text" placeholder="Nombre*">
                                        <input type="text" placeholder="Apellido*">
                                    </div>
                                    <div class="single-checkout-box">
                                        <select name="sex" id="sex-select">
                                            <option value="male">Masculino</option>
                                            <option value="female">Femenino</option>
                                        </select>
                                        <input type="text" placeholder="direcci�n*">
                                    </div>
                                    <div class="single-checkout-box">
                                        <input type="text" placeholder="Correo electr�nico*">
                                        <input type="text" placeholder="Celular*">
                                    </div>
                                    <div class="single-checkout-box select-option">
                                        <select name="country" id="country-select">
                                            <option value="sv">El Salvador</option>
                                            <option value="mx">M�xico</option>
                                        </select>
                                    </div>
                                    <div class="single-checkout-box checkbox">
                                        <input id="remind-me" type="checkbox">
                                        <label for="remind-me"><span></span>Crear una cuenta?</label>
                                    </div>
                                </div>
                            </div>
                            <!-- End Checkbox Area -->
                            <!-- Start Payment Box -->
                            <div class="payment-form">
                                <h2 class="section-title-3">Detalles del pago</h2>
                                <p>Sistema de pago seguro</p>
                                <div class="payment-form-inner">
                                    <div class="single-checkout-box">
                                        <input type="text" placeholder="Nombre en la tarjeta*" value="Alejandro Romero">
                                        <input type="text" placeholder="N�mero de tarjeta*" value="4000 0012 3456 7890">
                                    </div>
                                    <div class="single-checkout-box">
                                        <input type="text" placeholder="Fecha de vencimiento*" value="00/00">
                                        <input type="text" placeholder="C�digo de seguridad*" value="000">
                                    </div>
                                </div>
                            </div>
                            <!-- End Payment Box -->
                            <!-- Start Payment Way -->
                            <div class="our-payment-sestem">
                                <h2 class="section-title-3">Nosotros aceptamos :</h2>
                                <ul class="payment-menu">
                                    <li>
                                        <a href="#"><img src="images/payment/1.jpg" alt="payment-img"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="images/payment/2.jpg" alt="payment-img"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="images/payment/3.jpg" alt="payment-img"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="images/payment/4.jpg" alt="payment-img"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="images/payment/5.jpg" alt="payment-img"></a>
                                    </li>
                                </ul>
                                <div class="checkout-btn">
                                    <a class="ts-btn btn-light btn-large hover-theme" href="#">CONFIRMAR Y COMPRAR AHORA</a>
                                </div>
                            </div>
                            <!-- End Payment Way -->
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="puick-contact-area mt--60">
                            <h2 class="section-title-3">Contacto r�pido</h2>
                            <a href="phone:+8801722889963">+503 2256 05689</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Checkout Area -->
        <!-- Start Footer Area -->
        <%@include  file="footer.html" %>